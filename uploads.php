<?php

require('func.php');

/**
 * @param $dir
 */

function copyForDirectory($fileName)
{
    $dir = 'uploads/';
    $subDir = 'uploads/artigos/';
    $newName = null;

    if (is_file($subDir.$fileName)):
        $newName = generateNewFileName($subDir, $fileName);
        generateDirectory($subDir, $fileName, $newName);
    elseif (is_file($dir.$fileName)):
        $newName = generateNewFileName($dir, $fileName);
        generateDirectory($dir, $fileName, $newName);
    else:
        $error = 'arquivo '.$fileName. ' não existe.';
        Func::saveLogs($error);
    endif;

    return $newName;

}

function generateDirectory($dir, $fileName, $NewName)
{
    $newDir = createDirectoryName($NewName);
    $source = $dir . '/' . $fileName;
    $dist = 'uploadfiles3/' . $newDir . $NewName;
    $error = 'Arquivo ' . $fileName . ' convertido para ' . $dist;
    if (!is_dir('uploadfiles3/' . $newDir)):
        mkdir('uploadfiles3/' . $newDir, 0777, true);
        copy($source, $dist);
        Func::saveLogs($error);
    elseif(!empty($NewName)):
        copy($source, $dist);;
        Func::saveLogs($error);
    endif;

}

/**
 * @param $dir
 * @param $file
 * @return null|string
 */
function generateNewFileName($dir, $file)
{
    $formats = ['jpg', 'jpeg', 'png', 'gif'];

    $imgFormat = Func::imgFormat($file);

    if(in_array($imgFormat, $formats)):
        return strtolower(md5_file($dir . '/' .$file).
            '_'.str_replace('.', '', microtime(true)).
            '_'.mt_rand().
            '.'.$imgFormat);
    else:
        Func::saveLogs('Arquivo ' . $file . ' possui formato não permitido.');
        return null;
    endif;
}

/**
 * @param null $filename
 * @return string
 */
function createDirectoryName($filename = null){
    $f = str_split(substr($filename, 0, 4));
    return implode('/', $f).'/';
}