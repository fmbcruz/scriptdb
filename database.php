<?php
/*
 * Parameters of old database for connection
 */
define('HOST', 'localhost');
define('CHARSET', 'utf8');
define('USER', 'root');
define('PASSWORD', '12345');

class Database {

    /*
     * Static attribute for PDO instance
     */
    private static $pdo;

    /*
     * Hide a construct class
     */
    private function __construct() {
    }
    /**
     * This static method return a valid connection
     * Verify if exists a instance connection, if not, configure a new connection
     *
     * @param $host
     * @param $dbname
     * @param $charset
     * @param $user
     * @param $password
     * @return PDO
     */
    public static function getInstance($schema = null) {
        if (!isset(self::$pdo)) {
            try {
                $options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8', PDO::ATTR_PERSISTENT => TRUE);
                self::$pdo = new PDO(
                    "mysql:host=" . HOST . "; dbname=" . $schema . "; charset=" . CHARSET . ";", USER, PASSWORD, $options);
            } catch (PDOException $e) {
                print "Erro: " . $e->getMessage();
            }
        }
        return self::$pdo;
    }
}