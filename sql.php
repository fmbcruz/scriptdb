<?php

require_once 'database.php';

/**
 * Parameters of new database for connection
 */
define('HOST', 'localhost');
define('CHARSET', 'utf8');
define('USER', 'root');
define('PASSWORD', '12345');

class SQL {

    public static $sql;

    public static function select($schema = null, $table = null, $column = null, $where = null, $values = []) {

        $connection = Database::getInstance($schema);

        if(empty($column)):
            $sql  = "SELECT *";
        else:
            $sql = "SELECT " . $column;
        endif;

        $sql .= " FROM " . $table;

        if(!empty($where)):
            $sql .= " WHERE " . $where;
            $stm = $connection->prepare($sql);
            if(!empty($values)):
                foreach ($values as $key => $value):
                    $stm->bindValue($key, $value);
                endforeach;
            endif;
        else:
            $stm = $connection->prepare($sql);
        endif;

        $stm->execute() or die('<br>'.$sql.'<br>' . print_r($connection->errorInfo()));

        $connection = null;

        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function insert($schema = null, $table = null, $values = []){

        $conn = self::getConnection($schema);

        $sql     = "INSERT INTO " . $table;
        $columns = " (";
        $val    = " VALUES (";

        foreach ($values as $key => $value):
            $columns .= $key . ",";
            $val     .= "'" . $value ."',";
        endforeach;

        $columns = rtrim($columns,",") . ")";
        $val     = rtrim($val,",") . ")";

        if ($conn->query($sql . $columns . $val) === TRUE) {
            $last_id = $conn->insert_id;
        } else {
            echo $error = "Error: " . $sql . $columns . $val . "<br>" . $conn->error;
            Func::saveLogs($error);
            die();
        }

        $conn->close();

        return $last_id;

    }

    public static function update($schema = null, $table = null, $value = null, $where = null){

        $conn = self::getConnection($schema);

        $sql = "UPDATE " . $table . " SET " . $value . " WHERE " . $where;

        if ($conn->query($sql) === FALSE) {
            echo $error = "Error: " . $sql . "<br>" . $conn->error;
            Func::saveLogs($error);
            die();
        }

        $conn->close();

    }

    public static function getConnection($schema){
        $conn = new mysqli(HOST, USER, PASSWORD, $schema);
        $conn->set_charset("utf8");

        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        return $conn;
    }


}