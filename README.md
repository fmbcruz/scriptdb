# Script para migração de dados

Este projeto padroniza os dados do banco **boletim_production** para ser migrado para o banco **portal** de estrutura diferente. Converte, também, os arquivos de imagem para o padrão atual do portal.

# Arquivos

O projeto possui cinco arquivos (3 classes), sendo **migrate.php** o principal.

## Migrate

Responsável por executar a migração. Segue o seguinte fluxo:
- Seleciona as edições de **boletim_production > edicao**
- Gera valores para publish_date, number e year
- Insere o slug da edição em **portal > slug**
- Insere a capa da edição em **portal > file**
- Insere a edição em **portal > edition**
- Seleciona os artigos em **boletim_production > artigos**
- Insere o slug do artigo em **portal > slug**
- Insere a imagem do artigo em **portal > file**
- Insere o artigo em **portal > file**

O nome do banco de dados e o das tabelas já estão padronizados no arquivo. Se for necessário alterar as tabelas, basta alterar as variáveis:
- **$schema:** banco que será migrado
- **$schemaDist:** banco que receberá os dados

## Uploads
Responsável por converter o nome das imagens, gerar o diretório e salvá-las.

# Classes

## Database
Responsável por conexão com o banco de dados. Os parametros de conexão com o banco de dados **boletim_production** devem ser definidos no documento.

## SQL
Responsável por gerar as queries SQL e fazer as requisições. É importante informar os parâmetros de conexão com o banco de dados **portal** no documento.

## Func

Contém funções utilizadas para parametrizar dados, gerar slugs, retornar extensão de imagem, retirar caracteres especiais de string e geração de logs.


# Funcionamento

Representação esquemática do processo :

```mermaid
graph LR
A[boletim_production] -- SELECIONA EDICOES --> B{Parametrização}
A[boletim_production] -- SELECIONA ARTIGOS --> B{Parametrização}
B -- CRIA --> C(slug)
B -- CRIA --> D(imagem)
B -- EXPORTA --> E(edição / artigos)
C -- ID --> E
D -- ID --> E
E -- CRIA --> F[portal]
E -- SELECIONA ARTIGOS --> A

```

Pare realizar o processo de migração deve:
	> Configurar os parametros de conexão nos arquivos **sql.php** (portal) e **database.php** (boletim_production) .
	> Se necessário configura as variáveis do arquivo **migrate.php**:
- **$schemaDist:** tabela destino. (Padrão - portal)
-  **$author_id:** id do autor responsável pelas alterações. (Padrão - 3).
>Após as configurações, basta executar o arquivo **migrate.php**.

## Autor

Filipe Cruz - <filipembcruz@gmail.com>
01/02/2018