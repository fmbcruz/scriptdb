<?php
// Há 72 artigos onde as edições não estão na tabela

require('sql.php');
require('uploads.php');

/**
 * @ Table revista (edition)
 */
$schema = 'boletim_production';
$table  = 'revista';
$column = 'id, edicao, created_at, updated_at, capa';

$edition = SQL::select($schema, $table, '*');

$return = [];
$editArray = [];
$artArray = [];
$editionValues = [];
$i = 0;

$dhInicio = date("Y-m-d H:i:s");

while($i < count($edition)):

    $schemaDist = 'portal';
    $author_id  = 1; // author and user default (3)

    // Save Log
    Func::saveLogs('SELECT edicao ' . $edition[$i]['edicao'] . ' (ID = ' . $edition[$i]['id'] . ')');

    /**
     * Obtain the value of columns: publish_date, number, year
     */
    $conv = Func::convertValues($edition[$i]['edicao']);

    /**
     *  Insert the slug and return id
     */
    $slug = [
        'slug'        => Func::slugify($edition[$i]['edicao']),
        'post_type'   => 'edition',
    ];

    $slug_id = SQL::insert($schemaDist,'slug', $slug);

    Func::saveLogs('INSERT slug (edition | slug_id) ID = ' . $slug_id);

    $id_cover_file = null;
    $fileName = null;
    !empty($edition[$i]['capa']) ? $fileName = copyForDirectory($edition[$i]['capa']) : $id_cover_file = null;

    if(!empty($fileName)):

        $image = [
            'id_user'  => $author_id,
            'type'     => Func::imgFormat($edition[$i]['capa']),
            'filename' => $fileName,
            'title'    => $edition[$i]['capa'],
        ];

        $id_cover_file = SQL::insert($schemaDist,'file', $image);

        Func::saveLogs('INSERT file (edition | id_cover_file) ID = ' . $id_cover_file);

        $editionValues = array_merge($editionValues, [
            'id_periodical' => 1,
            'id_author'     => $author_id,
            'slug_id'       => $slug_id, // $slug_id
            'theme'         => $edition[$i]['edicao'],
            'status'        => 'published',
            'publish_date'  => $conv['publish_date'],
            'number'        => $conv['number'],
            'id_cover_file' => $id_cover_file,
            'year'          => $conv['year'],
            'created_at'    => $edition[$i]['created_at'],
            'updated_at'    => $edition[$i]['updated_at'],
        ]);
    else:
        $editionValues = array_merge($editionValues, [
            'id_periodical' => 1,
            'id_author'     => $author_id,
            'slug_id'       => $slug_id, // $slug_id
            'theme'         => $edition[$i]['edicao'],
            'status'        => 'published',
            'publish_date'  => $conv['publish_date'],
            'number'        => $conv['number'],
            'year'          => $conv['year'],
            'created_at'    => $edition[$i]['created_at'],
            'updated_at'    => $edition[$i]['updated_at'],
        ]);
    endif;

    $edition_id = SQL::insert($schemaDist,'edition', $editionValues);

    Func::saveLogs('INSERT edition ID = ' . $edition_id);

    SQL::update($schemaDist, 'slug', 'post_id = '.$edition_id, 'id='.$slug_id);

    Func::saveLogs('UPDATE slug ID = ' . $slug_id . ', post_id = ' . $edition_id);

    echo 'Edição ' . $edition[$i]['edicao'] . ', com os artigos:<br>';

    /**
     * @ Table artigos (articles)
     */
    $table  = 'artigos';
    $column = 'id, titulo, bigode, conteudo, autor, created_at, updated_at, imagem, edicao';
    $where  = 'edicao = ?';
    $values = [1 => $edition[$i]['id']];

    $artigos = SQL::select($schema, $table, $column, $where, $values);

    // Prepare vector with values of article
    $articleValues = [];
    $n = 0;
    $numArtigos = count($artigos);
    while($n < count($artigos)):

        Func::saveLogs('SELECT artigo ' . $artigos[$n]['titulo'] . ' (ID = ' . $artigos[$n]['id'] . ')');

        $slug = [
            'slug'        => Func::slugify($artigos[$n]['titulo']),
            'post_type'   => 'article',
        ];

        $slug_id = SQL::insert($schemaDist,'slug', $slug);

        Func::saveLogs('INSERT slug (articles | slug_id) ID = ' . $slug_id);

        $id_file_thumb = null;
        $fileNameArt = null;
        !empty($artigos[$n]['imagem']) ? $fileNameArt = copyForDirectory($artigos[$n]['imagem']) : $id_file_thumb = null;

        if(!empty($fileNameArt)):

            $image = [
                'id_user'  => $author_id,
                'type'     => Func::imgFormat($artigos[$n]['imagem']),
                'filename' => $fileNameArt,
                'title'    => $artigos[$n]['imagem'],
            ];

            $fileNameArt = null;

            $id_file_thumb = SQL::insert($schemaDist,'file', $image);

            Func::saveLogs('INSERT file (articles | id_file_thumb) ID = ' . $id_file_thumb);

            $articleValues = array_merge($articleValues, [
                'id_edition'    => $edition_id, // generate id above
                'id_author'     => $author_id,
                'title'         => Func::clearFormat($artigos[$n]['titulo']),
                'subtitle'      => Func::clearFormat($artigos[$n]['bigode']),
                'slug_id'       => $slug_id,
                'author_name'   => Func::clearFormat($artigos[$n]['autor']),
                'id_file_thumb' => $id_file_thumb,// generate image
                'content'       => Func::clearFormat($artigos[$n]['conteudo']),
                'created_at'    => $artigos[$n]['created_at'],
                'updated_at'    => $artigos[$n]['updated_at'],
            ]);
        else:
            $articleValues = array_merge($articleValues, [
                'id_edition'    => $edition_id, // generate id above
                'id_author'     => $author_id,
                'title'         => Func::clearFormat($artigos[$n]['titulo']),
                'subtitle'      => Func::clearFormat($artigos[$n]['bigode']),
                'slug_id'       => $slug_id,
                'author_name'   => Func::clearFormat($artigos[$n]['autor']),
                'content'       => Func::clearFormat($artigos[$n]['conteudo']),
                'created_at'    => $artigos[$n]['created_at'],
                'updated_at'    => $artigos[$n]['updated_at'],
            ]);
        endif;

        $article_id = SQL::insert($schemaDist,'article',$articleValues);

        Func::saveLogs('INSERT article ID = ' . $article_id);

        SQL::update($schemaDist, 'slug', 'post_id = '.$article_id, 'id='.$slug_id);

        Func::saveLogs('UPDATE slug ID = ' . $slug_id . ', post_id = ' . $article_id);

        echo '- ' . $artigos[$n]['titulo'] . '<br>';

        $articleValues = [];


        $n++;

    endwhile;

    $query .= ' OR edicao = ' . $edition[$i]['id'];

    $totalArtigos += $n;

    Func::saveLogs('Todos artigos da edição ' . $edition[$i]['edicao'] . ' (ID = ' . $edition[$i]['id'] . ') foram incluídos.' . PHP_EOL);


    echo 'Numero de arquivos: '  . $numArtigos . '<br>';
    echo 'Foi adicionada!<br><br>';

    $editionValues = [];

    $i++;

endwhile;

$dhFim = date("Y-m-d H:i:s");

$end = 'Foram adicionadas ' . $i . ' edições e ' . $totalArtigos . ' artigos <br>';
$end.= 'Tempo de processamento: ' . gmdate("H:i:s", (strtotime($dhFim) - strtotime($dhInicio)));
$end.= '<br>Mais detalhes no arquivo logs.txt<br>';

echo $end;

Func::saveLogs($end);